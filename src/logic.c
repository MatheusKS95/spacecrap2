#include "logic.h"

void loadmusic(enum e_music music, srt_gamecontent *content)
{
    if(IsMusicPlaying(content->soundtrack))
        UnloadMusicStream(content->soundtrack);

    if(music == MUS_MENU)
    {
        content->soundtrack = LoadMusicStream("gamedata/resources/soundtrack/Dark Descent.mp3");
        PlayMusicStream(content->soundtrack);
    }
    if(music == MUS_GAMEPLAY)
    {
        content->soundtrack = LoadMusicStream("gamedata/resources/soundtrack/SLAV.wav");
        PlayMusicStream(content->soundtrack);
    }
    if(music == MUS_ENDING_BAD)
    {
        content->soundtrack = LoadMusicStream("gamedata/resources/soundtrack/Whitesand - Melodic Dreams.mp3");
        PlayMusicStream(content->soundtrack);
    }
    if(music == MUS_ENDING_GOOD)
    {
        content->soundtrack = LoadMusicStream("gamedata/resources/soundtrack/through space.ogg");
        PlayMusicStream(content->soundtrack);
    }
}

void blyatman_damaged(srt_projectile projectile, srt_gamecontrols *controls, Sound sound)
{
    PlaySoundMulti(sound);
    if(controls->blyatman_status.ship_shield > 0)
    {
        switch(projectile.type)
        {
            case WPN_CANNON: controls->blyatman_status.ship_shield -= 5; break;
            case WPN_MINIGUN: controls->blyatman_status.ship_shield -= 1; break;
            case WPN_MISSILE: controls->blyatman_status.ship_shield -= 10; break;
            default: break;
        }
        if(controls->blyatman_status.ship_shield < 0)
            controls->blyatman_status.ship_shield = 0;
    }
    else if(controls->blyatman_status.ship_armour > 0)
    {
        switch(projectile.type)
        {
            case WPN_CANNON: controls->blyatman_status.ship_armour -= 5; break;
            case WPN_MINIGUN: controls->blyatman_status.ship_armour -= 1; break;
            case WPN_MISSILE: controls->blyatman_status.ship_armour -= 10; break;
            default: break;
        }
        if(controls->blyatman_status.ship_armour < 0)
            controls->blyatman_status.ship_armour = 0;
    }
    return;
}

void spacecrap_damaged(srt_projectile projectile, srt_gamecontrols *controls, Sound sound)
{
    PlaySoundMulti(sound);
    if(projectile.type == WPN_ANNIHILATOR)
    {
        controls->spacecrap_status.ship_armour = controls->spacecrap_status.ship_shield = 0;
    }
    else
    {
        if(controls->spacecrap_status.ship_shield > 0)
        {
            controls->spacecrap_status.ship_shield -= 20;
            if(controls->spacecrap_status.ship_shield < 0)
                controls->spacecrap_status.ship_shield = 0;
        }
        else if(controls->spacecrap_status.ship_armour > 0)
        {
            controls->spacecrap_status.ship_armour -= 20;
            if(controls->spacecrap_status.ship_armour < 0)
                controls->spacecrap_status.ship_armour = 0;
        }
    }
    return;
}

void addbullet(Vector3 position, bool enemy, srt_gamecontrols *controls, srt_gamecontent content)
{
    srt_projectile *projectile;
    projectile = malloc(sizeof(srt_projectile));

    if(projectile != NULL)
    {
        projectile->next = NULL;
        projectile->position = position;
        if(enemy)
            projectile->type = (unsigned int)GetRandomValue(WPNBM_MISSILE, WPN_ANNIHILATOR);
        else
            projectile->type = controls->weapon;
        projectile->enemy = enemy;

        if(projectile->enemy)
        {
            if(projectile->type == WPNBM_MISSILE)
            {
                projectile->speed = (Vector3){-1.0f, 0.0f, 0.0f};
                controls->gunfire.reload_bm = 10;
                PlaySoundMulti(content.gun_sounds.snd_bmmissile);
            }
            else if(projectile->type == WPN_ANNIHILATOR)
            {
                projectile->speed = (Vector3){-0.5f, 0.0f, 0.0f};
                controls->gunfire.reload_bm = 100;
                PlaySoundMulti(content.gun_sounds.snd_annihilator);
            }
        }
        else
        {
            if(projectile->type == WPN_CANNON)
            {
                projectile->speed = (Vector3){3.0f, 0.0f, 0.0f};
                controls->gunfire.reload_sc = 10;
            }
            else if(projectile->type == WPN_MINIGUN)
            {
                projectile->speed = (Vector3){10.0f, 0.0f, 0.0f};
                controls->gunfire.reload_sc = 1;
            }
            else if(projectile->type == WPN_MISSILE)
            {
                projectile->speed = (Vector3){1.0f, 0.0f, 0.0f};
                controls->gunfire.reload_sc = 30;
            }
        }

        if(controls->gunfire.first == NULL)
        {
            controls->gunfire.first = projectile;
        }
        else
        {
            controls->gunfire.last->next = projectile;
        }
        controls->gunfire.last = projectile;
    }
}

void removebullet(srt_projectile *bullet, srt_gamecontrols *controls)
{
    srt_projectile *prev, *aux;
    aux = controls->gunfire.first;
    prev = NULL;

    while(aux != NULL)
    {
        if(aux == bullet)
        {
            if(controls->gunfire.first == controls->gunfire.last)
            {
                controls->gunfire.first = controls->gunfire.last = NULL;
            }
            else
            {
                if(aux == controls->gunfire.first)
                    controls->gunfire.first = aux->next;
                else if(aux == controls->gunfire.last)
                {
                    prev->next = NULL;
                    controls->gunfire.last = prev;
                }
                else
                    prev->next = aux->next;
            }

            free(aux);
            return;
        }
        prev = aux;
        aux = aux->next;
    }
}

/*int bulletcount(srt_gunfire gunfire)
{
    srt_projectile *aux;
    int count = 0;
    aux = gunfire.first;
    while(aux != NULL)
    {
        count++;
        aux = aux->next;
    }
    return count;
}*/


void menu(srt_gamecontrols *controls, srt_gamecontent *content)
{
    UpdateCamera(&controls->camera_menu);
    content->buttonplay.x = 10.0f;
    content->buttonplay.y = SCREEN_HEIGHT / 2 + 50;
    content->buttonplay.width = 200.0f;
    content->buttonplay.height = 75.0f;
    content->buttonfullscrn.x = content->buttonplay.x;
    content->buttonfullscrn.y = content->buttonplay.y + 80;
    content->buttonfullscrn.width = 200.0f;
    content->buttonfullscrn.height = 50.0f;

    if(CheckCollisionPointRec(GetMousePosition(), content->buttonplay) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
    {
        controls->status = STS_INTRO;
        controls->st_game_started = true;
    }
    if(CheckCollisionPointRec(GetMousePosition(), content->buttonfullscrn) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
    {
        if(IsWindowFullscreen())
        {
            SetWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
            ToggleFullscreen();
        }
        else
        {
            int new_width = GetMonitorWidth(0);
            int new_height = GetMonitorHeight(0);
            SetWindowSize(new_width, new_height);
            ToggleFullscreen();
        }

    }
}

void intro(srt_gamecontrols *controls, srt_gamecontent *content)
{
    if(controls->st_game_started)
        loadmusic(MUS_GAMEPLAY, content);

    controls->st_game_started = false;
    if(IsKeyPressed(KEY_SPACE))
    {
        controls->status = STS_GAMEPLAY;
    }
}

void gameplay(srt_gamecontrols *controls, srt_gamecontent *content)
{
    UpdateCamera(&controls->camera_menu);

    if(IsKeyPressed(KEY_ONE))
    {
        controls->weapon = WPN_CANNON;
    }
    else if(IsKeyPressed(KEY_TWO))
    {
        controls->weapon = WPN_MISSILE;
    }
    else if(IsKeyPressed(KEY_THREE))
    {
        controls->weapon = WPN_MINIGUN;
    }

    if(IsKeyDown(KEY_SPACE) //SPACE TO SHOOT
            && controls->spacecrap_status.ship_armour > 0 //SC NEEDS TO BE ALIVE
            && controls->gunfire.reload_sc == 0 //COOLDOWN
            && !(controls->gunfire.minigun_jammed && controls->weapon == WPN_MINIGUN)) //CANNOT SHOOT IF MINIGUN IS JAMMED AFTER HEAT DAMAGE
    {
        Vector3 bulletposition = controls->sc_position_menu;
        bulletposition.x += 2.0f;
        addbullet(bulletposition, false, controls, *content);
        if(controls->weapon == WPN_MINIGUN)
        {
            controls->gunfire.minigun_jamcount++;
            PlaySoundMulti(content->gun_sounds.snd_gatling);
        }
        if(controls->weapon == WPN_MISSILE)
        {
            PlaySoundMulti(content->gun_sounds.snd_missile);
        }
        if(controls->weapon == WPN_CANNON)
        {
            PlaySoundMulti(content->gun_sounds.snd_cannon);
        }

        if(controls->gunfire.minigun_jamcount >= 100)
            controls->gunfire.minigun_jammed = true;
    }
    else if(IsKeyUp(KEY_SPACE) && controls->weapon == WPN_MINIGUN)
    {
        controls->gunfire.minigun_jamcount = 0;
    }

    if(IsKeyDown(KEY_LEFT) && controls->sc_position_menu.z > -40.0f)
    {
        controls->sc_position_menu.z -= 1.0f;
    }
    if(IsKeyDown(KEY_RIGHT) && controls->sc_position_menu.z < 40.0f)
    {
        controls->sc_position_menu.z += 1.0f;
    }
    if(IsKeyDown(KEY_UP) && controls->sc_position_menu.x < 60.0f)
    {
        controls->sc_position_menu.x += 1.0f;
    }
    if(IsKeyDown(KEY_DOWN) && controls->sc_position_menu.x >= 0.0f)
    {
        controls->sc_position_menu.x -= 1.0f;
    }

    if(controls->gunfire.reload_sc > 0)
        controls->gunfire.reload_sc--;
    if(controls->gunfire.reload_bm > 0)
        controls->gunfire.reload_bm--;

    if(controls->sc_position_menu.z > controls->bm_position.z)
        controls->bm_position.z += 0.4f;
    if(controls->sc_position_menu.z < controls->bm_position.z)
        controls->bm_position.z -= 0.4f;

    if((int)controls->sc_position_menu.z == (int)controls->bm_position.z
            && controls->blyatman_status.ship_armour > 0
            && controls->gunfire.reload_bm == 0)
        addbullet(controls->bm_position, true, controls, *content);

    if(controls->spacecrap_status.ship_armour <= 0)
    {
        controls->explosion_position = controls->sc_position_menu;
        controls->st_good_ending = false;
        controls->st_end = true;
    }
    else if(controls->blyatman_status.ship_armour <= 0)
    {
        controls->explosion_position = controls->bm_position;
        controls->st_good_ending = true;
        controls->st_end = true;
    }
    if((controls->spacecrap_status.ship_armour <= 0 || controls->blyatman_status.ship_armour <= 0) && !controls->st_explosion_active)
    {
        controls->st_explosion_active = true;

        controls->explosion_position.x -= controls->framewidth/2;
        controls->explosion_position.z -= controls->frameheight/2;
    }
    if(controls->st_explosion_active)
    {
        PlaySoundMulti(content->gun_sounds.snd_explosion_kill);
        controls->frames_counter += 8;
        if(controls->frames_counter > 16)
        {
            controls->current_exp_frame++;
            if(controls->current_exp_frame >= controls->num_frames_line)
            {
                controls->current_exp_frame = 0;
                controls->current_line++;
                if(controls->current_line >= controls->num_lines)
                {
                    controls->st_explosion_active = false;
                    controls->current_line = 0;
                    controls->status = STS_GAMEOVER;
                }
            }
            controls->frames_counter = 0;
        }
    }
    content->rectboom.x = controls->framewidth*controls->current_exp_frame;
    content->rectboom.y = controls->frameheight*controls->current_line;
    
    if(controls->frames_counter % 30 == 0 
		&& controls->blyatman_status.ship_shield < 100 
		&& controls->blyatman_status.ship_armour > 0)
    {
		controls->blyatman_status.ship_shield++;
	}
	if(controls->frames_counter % 50 == 0 
		&& controls->spacecrap_status.ship_shield < 100 
		&& controls->spacecrap_status.ship_armour > 0)
    {
		controls->spacecrap_status.ship_shield++;
	}
}

void ending(srt_gamecontrols *controls, srt_gamecontent *content)
{
    if(controls->st_end)
    {
        if(controls->st_good_ending)
            loadmusic(MUS_ENDING_GOOD, content);
        else
            loadmusic(MUS_ENDING_BAD, content);
    }
    controls->st_end = false;
}

void loop(srt_gamecontrols *controls, srt_gamecontent *content)
{
    UpdateMusicStream(content->soundtrack);
    switch(controls->status)
    {
        case STS_MENU: menu(controls, content); break;
        case STS_INTRO: intro(controls, content); break;
        case STS_GAMEPLAY: gameplay(controls, content); break;
        case STS_GAMEOVER: ending(controls, content); break;
        default: break;
    }
}
