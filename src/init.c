#include "init.h"

void load_strings(enum e_lang language, srt_gamecontent *content)
{
    char *full_string;
    char *aux_strings[15];
    int counter = 0;
    if(language == LANG_EN)
    {
        full_string = LoadFileText("gamedata/resources/translations/EN.txt");
    }
    else if(language == LANG_PT)
    {
        full_string = LoadFileText("gamedata/resources/translations/PTBR.txt");
    }
    else
    {
        full_string = LoadFileText("gamedata/resources/translations/EN.txt");
    }
    aux_strings[counter] = strtok(full_string, "|");
    while(aux_strings[counter] != NULL)
    {
        aux_strings[++counter] = strtok(NULL, "|");
    }
    content->strings.str_play = aux_strings[0];
    content->strings.str_enable_fullscreen = aux_strings[1];
    content->strings.str_disable_fullscreen = aux_strings[2];
    content->strings.str_notice = aux_strings[3];
    content->strings.str_intro = aux_strings[4];
    content->strings.str_space_continue = aux_strings[5];
    content->strings.str_game_shield = aux_strings[6];
    content->strings.str_game_armour = aux_strings[7];
    content->strings.str_game_cannon = aux_strings[8];
    content->strings.str_game_gatling = aux_strings[9];
    content->strings.str_game_missile = aux_strings[10];
    content->strings.str_victory = aux_strings[11];
    content->strings.str_defeat = aux_strings[12];
    content->strings.str_credits = aux_strings[13];
}

void init_game()
{
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "SpaceCrap 2");
    InitAudioDevice();
}

void load_content(srt_gamecontent *content)
{
    //loading localized strings
    load_strings(LANG_EN, content);

    //loading 3d models
    content->spacecrap = LoadModel("gamedata/resources/models/SmallSpaceship.obj");
    content->blyatman = LoadModel("gamedata/resources/models/LongSpaceship.obj");

    //loading images and textures
    content->title = LoadTexture("gamedata/resources/menu/title.png");
    content->spacecrap.materials[0].maps[MAP_DIFFUSE].texture = LoadTexture("gamedata/resources/models/SmallSpaceshipTexture.png");
    content->blyatman.materials[0].maps[MAP_DIFFUSE].texture = LoadTexture("gamedata/resources/models/LongSpaceshipTexture.png");
    content->texture_billboard = LoadTexture("gamedata/resources/textures/skybox.png");
    content->explosion = LoadTexture("gamedata/resources/textures/exp2_0.png");


    //loading sound effects
    content->gun_sounds.snd_missile = LoadSound("gamedata/resources/sounds/rlaunch.wav");
    content->gun_sounds.snd_gatling = LoadSound("gamedata/resources/sounds/P_16P.wav");
    content->gun_sounds.snd_cannon = LoadSound("gamedata/resources/sounds/M_26P.wav");
    content->gun_sounds.snd_bmmissile = LoadSound("gamedata/resources/sounds/Laser_00.wav");
    content->gun_sounds.snd_annihilator = LoadSound("gamedata/resources/sounds/iceball.wav");
    content->gun_sounds.snd_explosion_hit = LoadSound("gamedata/resources/sounds/explodemini.wav");
    content->gun_sounds.snd_explosion_kill = LoadSound("gamedata/resources/sounds/explode.wav");

    //loading other content files
    content->soundtrack = LoadMusicStream("gamedata/resources/soundtrack/Dark Descent.mp3"); //initialization, required after globals removed
    content->font = LoadFontEx("gamedata/resources/fonts/Teko-SemiBold.ttf", 72, 0, 250);
}

void initial_setup(srt_gamecontent *content, srt_gamecontrols *controls)
{
    //models positions
    controls->sc_position_menu = (Vector3){0.0f, 0.0f, 0.0f};
    controls->bm_position = (Vector3){100.0f, 0.0f, 15.0f};

    //cameras
    controls->camera_menu.up = (Vector3){0.0f, 1.0f, 0.0f};
    controls->camera_menu.fovy = 45.0f;
    controls->camera_menu.target = (Vector3){0.0f, 0.0f, 0.0f};
    controls->camera_menu.position = (Vector3){10.0f, 10.0f, 10.0f};
    controls->camera_menu.type = CAMERA_PERSPECTIVE;

    //ship status
    controls->spacecrap_status = (srt_ship_status){100, 100};
    controls->blyatman_status = (srt_ship_status){100, 100};

    //shooting spree
    controls->gunfire.first = NULL;
    controls->gunfire.last = NULL;
    controls->gunfire.reload_sc = 5;
    controls->gunfire.reload_bm = 15;
    controls->gunfire.minigun_jammed = false;
    controls->gunfire.minigun_jamcount = 0;

    //selected weapon
    controls->weapon = WPN_CANNON;

    //explosions
    controls->num_frames_line = 4;
    controls->num_lines = 4;
    controls->framewidth = content->explosion.width/controls->num_frames_line;
    controls->frameheight = content->explosion.height/controls->num_lines;
    controls->current_exp_frame = 0;
    controls->current_line = 0;
    content->rectboom.height = controls->frameheight;
    content->rectboom.width = controls->framewidth;
    content->rectboom.x = 0;
    content->rectboom.y = 0;

    //current game status
    controls->status = STS_MENU;

    //other controls
    controls->frames_counter = 0;
    controls->st_game_started = false;
    controls->st_good_ending = false;
    controls->st_end = false;
    controls->st_explosion_active = false;

    SetCameraMode(controls->camera_menu, CAMERA_ORBITAL);
    SetTargetFPS(60);
}
