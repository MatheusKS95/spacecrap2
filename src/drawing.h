#include "logic.h"

#ifndef DRAWING_H
#define DRAWING_H

void drawbullets(srt_gamecontrols *controls, srt_gamecontent content);
void drawmenu(srt_gamecontrols controls, srt_gamecontent content);
void drawintro(srt_gamecontrols controls, srt_gamecontent content);
void drawgame(srt_gamecontrols *controls, srt_gamecontent content);
void drawend(srt_gamecontrols controls, srt_gamecontent content);
void draw(srt_gamecontrols *controls, srt_gamecontent content);

#endif // DRAWING_H
