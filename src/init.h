#include <string.h>
#include "common.h"

#ifndef INIT_H
#define INIT_H

void load_strings(enum e_lang language, srt_gamecontent *content);
void init_game();
void load_content(srt_gamecontent *content);
void initial_setup(srt_gamecontent *content, srt_gamecontrols *controls);

#endif // INIT_H
