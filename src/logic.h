#include "init.h"

#ifndef LOGIC_H
#define LOGIC_H

void loadmusic(enum e_music music, srt_gamecontent *content);
void blyatman_damaged(srt_projectile projectile, srt_gamecontrols *controls, Sound sound);
void spacecrap_damaged(srt_projectile projectile, srt_gamecontrols *controls, Sound sound);
void addbullet(Vector3 position, bool enemy, srt_gamecontrols *controls, srt_gamecontent content);
void removebullet(srt_projectile *bullet, srt_gamecontrols *controls);
int bulletcount(srt_gunfire gunfire);
void menu(srt_gamecontrols *controls, srt_gamecontent *content);
void intro(srt_gamecontrols *controls, srt_gamecontent *content);
void gameplay(srt_gamecontrols *controls, srt_gamecontent *content);
void ending(srt_gamecontrols *controls, srt_gamecontent *content);
void loop(srt_gamecontrols *controls, srt_gamecontent *content);

#endif // LOGIC_H
