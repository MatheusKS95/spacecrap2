/*
SpaceCrap 2 - a 3D space shooter game using C99 and Raylib
Copyright (C) 2020 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "drawing.h"

void unload_content(srt_gamecontent *content)
{
    UnloadTexture(content->title);
    UnloadFont(content->font);
    UnloadModel(content->spacecrap);
    UnloadModel(content->blyatman);
    UnloadTexture(content->texture_billboard);
    UnloadTexture(content->explosion);
    UnloadSound(content->gun_sounds.snd_missile);
    UnloadSound(content->gun_sounds.snd_cannon);
    UnloadSound(content->gun_sounds.snd_gatling);
    UnloadSound(content->gun_sounds.snd_bmmissile);
    UnloadSound(content->gun_sounds.snd_annihilator);
    UnloadSound(content->gun_sounds.snd_explosion_hit);
    UnloadSound(content->gun_sounds.snd_explosion_kill);
    CloseAudioDevice();
    CloseWindow();
}

int main()
{
    srt_gamecontent game_content;
    srt_gamecontrols game_controls;
    game_content.strings = (srt_strings){"","","","","","","","","","","","","","",""};
    init_game();
    load_content(&game_content);
    initial_setup(&game_content, &game_controls);
    loadmusic(MUS_MENU, &game_content);

    while(!WindowShouldClose())
    {
        loop(&game_controls, &game_content);
        game_controls.frames_counter += 8;

        if(game_controls.status != STS_MENU)
        {
            game_controls.camera_menu.up = (Vector3){0.0f, 1.0f, 0.0f};
            game_controls.camera_menu.fovy = 45.0f;
            game_controls.camera_menu.target = (Vector3){0.0f, 20.0f, 0.0f};
            game_controls.camera_menu.position = (Vector3){-55.0f, 45.0f, 0.0f};
            HideCursor();
        }
        draw(&game_controls, game_content);
    }
    unload_content(&game_content);

    return 0;
}
