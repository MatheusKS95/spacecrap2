/*
SpaceCrap 2 - a 3D space shooter game using C99 and Raylib
Copyright (C) 2020 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <raylib.h>
//raylib.h also includes stdbool.h

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#ifndef COMMON_H
#define COMMON_H

//ENUMERATORS START HERE
/*
 * Enum
 * e_gamestatus
 * Used for telling the drawing and logic functions which point of game the player is
 * > STS_MENU = currently on main menu
 * > STS_INTRO = on introductory screen (the one that tells the backstory between the earlier SDL2 game and this one)
 * > STS_GAMEPLAY = the game is on the main part, where the battle against the antagonist occurs
 * > STS_GAMEOVER = the ending screen
*/
enum e_gamestatus
{
    STS_MENU,
    STS_INTRO,
    STS_GAMEPLAY,
    STS_GAMEOVER
};

/*
 * Enum
 * e_music
 * Used to tell loadmusic function which song must be played
 * > MUS_MENU = the menu theme
 * > MUS_GAMEPLAY = the battle theme
 * > MUS_ENDING_GOOD = credits theme if the antagonist is defeated
 * > MUS_ENDING_BAD = credtis theme if the player is defeated
*/
enum e_music
{
    MUS_MENU,
    MUS_GAMEPLAY,
    MUS_ENDING_GOOD,
    MUS_ENDING_BAD
};

/*
 * Enum
 * e_lang
 * Controls which language is used when loading string contents
 * Currently supports only LANG_EN (English) and LANG_PT (Portuguese)
*/
enum e_lang
{
    LANG_EN,
    LANG_PT
};

/*
 * Enum
 * e_weapon
 * Tells the logic functions and the bullet drawing functions which weapon are being used and identify bullet type
 * > WPN_CANNON = Player's cannon
 * > WPN_MISSILE = Player's missile
 * > WPN_MINIGUN = Player's minigun
 * > WPNBM_MISSILE = Antagonist's missile
 * > WPN_ANNIHILATOR = Antagonist's event horizon-type weapon. Getting hit by it means instakill
*/
enum e_weapon
{
    WPN_CANNON,
    WPN_MISSILE,
    WPN_MINIGUN,
    WPNBM_MISSILE,
    WPN_ANNIHILATOR
};

//ENUMERATORS END HERE

//STRUCTS START HERE
/*
 * Struct
 * srt_strings
 * Each member holds a string in a selected language
*/
typedef struct srt_strings
{
    char *str_play;
    char *str_enable_fullscreen;
    char *str_disable_fullscreen;
    char *str_notice;
    char *str_intro;
    char *str_space_continue;
    char *str_keys_help;
    char *str_game_shield;
    char *str_game_armour;
    char *str_game_cannon;
    char *str_game_gatling;
    char *str_game_missile;
    char *str_victory;
    char *str_defeat;
    char *str_credits;
} srt_strings;

/*
 * Struct
 * srt_ship_status
 * Used to keep track of ship health. Victory or defeat conditions are based on it's current value. Decrement only.
 * The side that gets both 0 or negative value on both members lose.
 * > ship_shield = Integer value. Ship's non-solid deflector shield. When <= 0 it stops decrementing.
 * > ship_armour = Integer value. Ship's physical solid hull armour. Starts decrementing when ship_shield gets <= 0. When
 *      ship_armour also gets <= 0 the ship explodes
*/
typedef struct srt_ship_status
{
    int ship_shield;
    int ship_armour;
} srt_ship_status;

/*
 * Struct
 * srt_projectile
 * Represents a single projectile in a linked list.
 * > position = Vector3 value. Holds the current projectile position in space.
 * > speed = Vector3 value. Delta which is used to calculate projectile's next position.
 * > type = e_weapon enum value. Identify the projectile's type (missile/minigun rounds/etc).
 * > next = pointer. Tells where's the next projectile.
 * > enemy = boolean (C99). Tells if the bullet is yours or not.
*/
typedef struct srt_projectile
{
    Vector3 position;
    Vector3 speed;
    enum e_weapon type;
    struct srt_projectile *next;
    bool enemy;
} srt_projectile;

/*
 * Struct
 * srt_gunfire
 * Linked list of projectiles.
 * > first = pointer. The first projectile on list.
 * > last = pointer. Last projectile added on list.
 * > reload_sc = integer. Used as a cooldown for the player's weapons. The value is added and subtracted
 *      in order to prevent a massive swarm of bullets.
 * > reload_bm = integer. Cooldown for enemy's weapons.
 * > minigun_jammed = bool. Used to lock the gatling gun and prevents it's use after being fired for too long.
 * > minigun_jamcount = integer. Incremented when player hold the firing key pressed when using the gatling
 *      gun. Zeroed when player releases the key. Used to prevent infinite gatling.
*/
typedef struct srt_gunfire
{
    struct srt_projectile *first;
    struct srt_projectile *last;
    int reload_sc;
    int reload_bm;
    bool minigun_jammed;
    int minigun_jamcount;
} srt_gunfire;

/*
 * Struct
 * srt_gunsounds
 * Stores the sound effects for weapons.
 * Members are raylib's sound struct. Plays when fired.
*/
typedef struct srt_gunsounds
{
    Sound snd_missile;
    Sound snd_cannon;
    Sound snd_gatling;
    Sound snd_bmmissile;
    Sound snd_annihilator;
    Sound snd_explosion_hit;
    Sound snd_explosion_kill;
} srt_gunsounds;

/*
 * Struct
 * srt_gamecontent
 * Stores content used across the game, like 3D models, textures, sounds...
*/
typedef struct srt_gamecontent
{
    //The following ones are design-related
    Font font;
    Texture2D title;
    Rectangle buttonplay;
    Rectangle buttonfullscrn;
    Texture2D texture_billboard;
    Music soundtrack;

    //The following ones are models-related
    Model spacecrap;
    Model blyatman;

    //The following ones are design-related but exclusive to explosion effects
    Texture2D explosion;
    Rectangle rectboom;

    srt_gunsounds gun_sounds;

    //The following one stores the localised strings
    srt_strings strings;
} srt_gamecontent;

/*
 * Struct
 * srt_gamecontrols
 * Stores game status and variable controls during it's execution. Also includes positioning and cameras
*/
typedef struct srt_gamecontrols
{
    //The following ones are used to keep track of models' positions
    Vector3 sc_position_menu;
    Vector3 bm_position;

    //The following ones are camera-related
    Camera3D camera_menu;

    //The following ones are game logic controls
    srt_ship_status spacecrap_status;
    srt_ship_status blyatman_status;
    enum e_weapon weapon;
    enum e_gamestatus status;
    int frames_counter;
    bool st_game_started;
    bool st_good_ending;
    bool st_end;

    //The following ones are design-related but exclusive to explosion effects
    bool st_explosion_active;
    Vector3 explosion_position;
    int num_frames_line;
    int num_lines;
    int framewidth;
    int frameheight;
    int current_exp_frame;
    int current_line;

    //gunfire list
    srt_gunfire gunfire;
} srt_gamecontrols;

//STRUCTS END HERE

#endif // COMMON_H
