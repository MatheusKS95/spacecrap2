#include "drawing.h"

void drawbullets(srt_gamecontrols *controls, srt_gamecontent content)
{
    srt_projectile *aux = controls->gunfire.first;

    while(aux != NULL)
    {
        Vector3 new_pos = aux->position;
        new_pos.x += aux->speed.x;
        new_pos.y += aux->speed.y;
        new_pos.z += aux->speed.z;
        aux->position = new_pos;

        switch(aux->type)
        {
            case WPN_CANNON:
                DrawSphere(aux->position, 0.3f, GOLD);
                break;
            case WPN_MINIGUN:
                DrawCube(aux->position, 0.3f, 0.2f, 0.2f, GOLD);
                break;
            case WPN_MISSILE:
                DrawSphere(aux->position, 0.3f, SKYBLUE);
                break;
            case WPNBM_MISSILE:
                DrawSphere(aux->position, 0.3f, RED);
                break;
            case WPN_ANNIHILATOR:
                DrawSphere(aux->position, 0.3f, GREEN);
                break;
            default: break;
        }

        if(aux->position.x >= 500.0f || aux->position.x < -50.0f)
            removebullet(aux, controls);

        if(!aux->enemy && controls->blyatman_status.ship_armour > 0 && CheckCollisionBoxSphere(
                    (BoundingBox){(Vector3){controls->bm_position.x - 4.0f, controls->bm_position.y - 4.0f, controls->bm_position.z - 4.0f},
                                  (Vector3){controls->bm_position.x + 4.0f, controls->bm_position.y + 4.0f, controls->bm_position.z + 4.0f}},
                    aux->position, 0.3f))
        {
            blyatman_damaged(*aux, controls, content.gun_sounds.snd_explosion_hit);
            removebullet(aux, controls);
        }

        if(aux->enemy && controls->spacecrap_status.ship_armour > 0 && CheckCollisionBoxSphere(
                    (BoundingBox){(Vector3){controls->sc_position_menu.x - 2.0f, controls->sc_position_menu.y - 2.0f, controls->sc_position_menu.z - 2.0f},
                                  (Vector3){controls->sc_position_menu.x + 2.0f, controls->sc_position_menu.y + 2.0f, controls->sc_position_menu.z + 2.0f}},
                    aux->position, 0.1f))
        {
            spacecrap_damaged(*aux, controls, content.gun_sounds.snd_explosion_hit);
            removebullet(aux, controls);
        }

        aux = aux->next;
    }
}

void drawmenu(srt_gamecontrols controls, srt_gamecontent content)
{
    BeginMode3D(controls.camera_menu);
        DrawModelWires(content.spacecrap, controls.sc_position_menu, 1.0f, WHITE);
    EndMode3D();
    DrawTexture(content.title, ((GetScreenWidth() / 2) - (content.title.width / 2)), 10, WHITE);
    if(CheckCollisionPointRec(GetMousePosition(), content.buttonplay))
    {
        DrawRectangleRec(content.buttonplay, DARKGRAY);
    }
    else
    {
        DrawRectangleRec(content.buttonplay, BLACK);
    }
    if(CheckCollisionPointRec(GetMousePosition(), content.buttonfullscrn))
    {
        DrawRectangleRec(content.buttonfullscrn, DARKGRAY);
    }
    else
    {
        DrawRectangleRec(content.buttonfullscrn, BLACK);
    }
    DrawTextEx(content.font, content.strings.str_play, (Vector2){content.buttonplay.x + 10, content.buttonplay.y + 3}, 72, 1, WHITE);
    if(!IsWindowFullscreen())
        DrawTextEx(content.font, content.strings.str_enable_fullscreen, (Vector2){content.buttonfullscrn.x + 10, content.buttonfullscrn.y + 2}, 48, 1, WHITE);
    else
        DrawTextEx(content.font, content.strings.str_disable_fullscreen, (Vector2){content.buttonfullscrn.x + 10, content.buttonfullscrn.y + 2}, 48, 1 , WHITE);
    DrawTextEx(content.font, content.strings.str_notice, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_notice, 24, 1).x / 2)), GetScreenHeight() - 40}, 24, 1, WHITE);
}

void drawintro(srt_gamecontrols controls, srt_gamecontent content)
{
    DrawTextEx(content.font, TextSubtext(content.strings.str_intro, 0, controls.frames_counter / 10), (Vector2){10.0f, 10.0f}, 36, 1, WHITE);
}

void drawgame(srt_gamecontrols *controls, srt_gamecontent content)
{
    BeginMode3D(controls->camera_menu);
        DrawBillboard(controls->camera_menu, content.texture_billboard, (Vector3){300.0f, -20.0f, 0.0f}, 800.0f, WHITE);
        if(controls->spacecrap_status.ship_armour > 0)
            DrawModelWiresEx(content.spacecrap, controls->sc_position_menu, (Vector3){0.4f, 0.0f, 0.0f}, 1.0f, (Vector3){1.0f, 1.0f, 1.0f}, WHITE);
        if(controls->blyatman_status.ship_armour > 0)
            DrawModelWiresEx(content.blyatman, controls->bm_position, (Vector3){0.0f, 1.0f, 0.0f}, -90.0f, (Vector3){4.0f, 4.0f, 4.0f}, WHITE);
        drawbullets(controls, content);
        if(controls->st_explosion_active)
            DrawBillboardRec(controls->camera_menu, content.explosion, content.rectboom, controls->explosion_position, 40.0f, WHITE);
    EndMode3D();
    DrawTextEx(content.font, TextFormat("Blyatman %s %d%% | %s %d%%", content.strings.str_game_shield, controls->blyatman_status.ship_shield, content.strings.str_game_armour, controls->blyatman_status.ship_armour), (Vector2){10.0f, 10.0f}, 24, 1, WHITE);
    DrawTextEx(content.font, TextFormat("SpaceCrap %s %d%% | %s %d%%", content.strings.str_game_shield, controls->spacecrap_status.ship_shield, content.strings.str_game_armour, controls->spacecrap_status.ship_armour), (Vector2){GetScreenWidth() - 10 - MeasureTextEx(content.font, TextFormat("SpaceCrap %s %d%% | %s %d%%", content.strings.str_game_shield, controls->spacecrap_status.ship_shield, content.strings.str_game_armour, controls->spacecrap_status.ship_armour), 24, 1).x, 10.0f}, 24, 1, WHITE);
    switch(controls->weapon)
    {
        case WPN_CANNON:
            DrawTextEx(content.font, content.strings.str_game_cannon, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_game_cannon, 24, 1).x / 2)), GetScreenHeight() - 40.0f}, 24.0f, 1.0f, GREEN);
            break;
        case WPN_MISSILE:
            DrawTextEx(content.font, content.strings.str_game_missile, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_game_missile, 24, 1).x / 2)), GetScreenHeight() - 40.0f}, 24.0f, 1.0f, GREEN);
            break;
        case WPN_MINIGUN:
            if(controls->gunfire.minigun_jammed)
                DrawTextEx(content.font, content.strings.str_game_gatling, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_game_gatling, 24, 1).x / 2)), GetScreenHeight() - 40.0f}, 24.0f, 1.0f, RED);
            else if(controls->gunfire.minigun_jamcount > 70)
                DrawTextEx(content.font, content.strings.str_game_gatling, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_game_gatling, 24, 1).x / 2)), GetScreenHeight() - 40.0f}, 24.0f, 1.0f, ORANGE);
            else
                DrawTextEx(content.font, content.strings.str_game_gatling, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_game_gatling, 24, 1).x / 2)), GetScreenHeight() - 40.0f}, 24.0f, 1.0f, GREEN);
            break;
        default: break;
    }
    DrawTextEx(content.font, TextFormat("FPS: %d", GetFPS()), (Vector2){10.0f, GetScreenHeight() - 24.0f}, 24.0f, 1.0f, WHITE);
}

void drawend(srt_gamecontrols controls, srt_gamecontent content)
{
    if(controls.st_good_ending)
        DrawTextEx(content.font, content.strings.str_victory, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_victory, 48.0f, 1.0f).x / 2)), 10.0f}, 48.0f, 1.0f, WHITE);
    else
        DrawTextEx(content.font, content.strings.str_defeat, (Vector2){((GetScreenWidth() / 2) - (MeasureTextEx(content.font, content.strings.str_defeat, 48.0f, 1.0f).x / 2)), 10.0f}, 48.0f, 1.0f, WHITE);

    DrawTextEx(content.font, content.strings.str_credits, (Vector2){10.0f, 60.0f}, 20.0f, 1.0f, WHITE);
}

void draw(srt_gamecontrols *controls, srt_gamecontent content)
{
    BeginDrawing();
        ClearBackground(BLACK);
        switch(controls->status)
        {
            case STS_MENU:
                drawmenu(*controls, content);
                break;
            case STS_INTRO:
                drawintro(*controls, content);
                break;
            case STS_GAMEPLAY:
                drawgame(controls, content);
                break;
            case STS_GAMEOVER:
                drawend(*controls, content);
                break;
            default:
                break;
        }
    EndDrawing();
}
