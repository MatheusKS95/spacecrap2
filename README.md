# SpaceCrap 2

SpaceCrap 2 is a simple 3D space shooter video game made using C99 and [Raylib](https://www.raylib.com).

## Building

SpaceCrap is built using CMake (at least version 3.1 ONLY). Raylib is required (installed on your system as a shared object [as explained here](https://github.com/raysan5/raylib/wiki/Working-on-GNU-Linux)). To build the game, use the following commands on the game's directory:

```mkdir build```

```cd build```

```cmake ..```

```make```

Then copy the gamedata folder into build directory (so the game can run properly with all the content). Remember: you NEED to have Raylib in your system, as the script today don't fetch it automatically (I still have issues with it).

Currently SpaceCrap 2 doesn't support other platforms (Windows, OS X, Android, etc.). One way to make it work on Windows is following the Raylib's guide [here](https://github.com/raysan5/raylib/wiki/Working-on-Windows).
Otherwise, working on Windows as-is will require Cygwin or MSYS2 (following the same instructions as Linux).

## Contributing
Please, keep in mind this is a WIP. If you want to contribute, please open a new branch and when done submit a pull request.


## License
[GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)
